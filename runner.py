import postprocessing as pp


class MainRunner:

    def __init__(self, data, posts_lengths, pipeline):
        self.data = data
        self.posts_lengths = posts_lengths
        self.pipeline = pipeline

    def run(self, **kwargs):
        reports = list()
        for posts_length in self.posts_lengths:
            data = self.data.get_first_n_posts(posts_length)

            X_train, X_test, Y_train, Y_test = data.train.X, data.test.X, data.train.Y, data.test.Y

            print(f"Started training for {posts_length} posts.")
            self.pipeline.fit(X_train, Y_train)

            Y_predicted = self.pipeline.predict(X_test)
            report = pp.get_report(Y_test, Y_predicted,
                                   output_dict=True, labels=[0, 1], target_names=["negative", "positive"])
            print(report)
            reports.append(report)

        return reports
