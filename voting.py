import postprocessing as pp


def majority_voting(data, model):
    Y_predictions = []
    Y_true = []

    for user, label in zip(data.test.X, data.test.Y):
        if not len(user.posts):
            continue

        Y_true.append(label)
        positive, negative = 0, 0
        for chunk in user.posts:
            Y_predicted = model.predict([chunk])
            if Y_predicted:
                positive += 1
            else:
                negative += 1

        Y_predictions.append(1 if positive >= negative else 0)

        print(f'Label = {label} Positive chunks = {positive} Negative chunks = {negative}')

    report = pp.get_report(Y_true, Y_predictions,
                           output_dict=True, labels=[0, 1], target_names=["negative", "positive"])

    return report


def average_predict_proba_voting(data, model):
    Y_predictions = []
    Y_true = []

    for user, label in zip(data.test.X, data.test.Y):
        if not len(user.posts):
            continue

        Y_true.append(label)
        probs = list()
        for chunk in user.posts:
            prob = model.predict_proba([chunk])
            probs.append(prob[0][1])

        mean_prob = sum(probs) / len(probs)
        Y_predictions.append(1 if mean_prob >= 0.5 else 0)

    report = pp.get_report(Y_true, Y_predictions,
                           output_dict=True, labels=[0, 1], target_names=["negative", "positive"])

    return report
