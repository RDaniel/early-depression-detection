import matplotlib.pyplot as plt
from sklearn.metrics import classification_report

get_report = classification_report


def graph_reports(reports, measures, names, x=None):
    """Plot chosen measures from reports for each provided name."""
    if not x:
        x = list(range(len(reports)))

    fig, axes = plt.subplots(ncols=len(measures))
    ys = {name: [] for name in names}

    for measure, ax in zip(measures, (axes if getattr(axes, "__iter__", False) else (axes,))):
        for report in reports:
            for name in names:
                ys[name].append(report[name][measure])
        for name in names:
            ax.title.set_text(measure)
            ax.plot(x, ys[name])
            ys[name] = []

    fig.legend(names)
    plt.show()


def report_string(report):
    string = [" " * 17, "-" * 15, "", "", "", "", "-" * 15, "=" * 15]
    size = 15
    from_b = 15
    for key in sorted(report.keys()):
        if key != "accuracy":
            string[0] += "{:15s}".format(key)
            string[1] += "-" * size
            string[-2] += "-" * size
            string[-1] += "=" * size
            for i, key2 in enumerate(sorted(report[key].keys())):
                if string[i + 2] == "":
                    string[i + 2] += "{:10}".format(key2)
                string[i + 2] += "{:15f}".format(report[key][key2])
        else:
            string[-1] = f"{key}: {report[key]}\n" + string[-1]
    return "\n".join(string)


if __name__ == "__main__":
    y1 = [0, 0, 0, 0, 1, 1, 0, 0]
    y1_pred = [1, 0, 1, 0, 1, 0, 1, 1]
    r1 = get_report(y1, y1_pred, output_dict=True, labels=[0, 1], target_names=["negative", "positive"])
    y2 = [1, 1, 1, 1, 1, 1, 1, 1]
    y2_pred = [1, 1, 1, 0, 1, 1, 0, 0]
    r2 = get_report(y2, y2_pred, output_dict=True, labels=[0, 1], target_names=["negative", "positive"])
    graph_reports([r1, r2], ["precision", "recall"], ["negative", "positive"])
    print(report_string(r2))
