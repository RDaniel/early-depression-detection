import os
import xml.etree.ElementTree as ET

import model


def load_from_directory(directory_path):
    users = []

    for file_name in os.listdir(directory_path):
        users.append(load_from_file(os.path.join(directory_path, file_name)))

    return users


def load_from_file(file_name):
    parse_tree = ET.parse(file_name)
    root = parse_tree.getroot()
    children = root.getchildren()

    user_id = children[0].text
    posts = []

    for post in root.getchildren()[1:]:
        post_elements = post.getchildren()
        posts.append(model.Post(title=post_elements[0].text.strip(),
                                date=post_elements[1].text.strip(),
                                text=post_elements[3].text.strip()))

    return model.User(user_id, posts)
