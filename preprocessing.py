from abc import ABC, abstractmethod

import spacy


class Preprocessor:

    def __init__(self, model='en_core_web_sm', batch_size=20):
        self.nlp = spacy.load(model)
        self.batch_size = batch_size

    def __docs(self, texts, disable, ):
        return self.nlp.pipe(texts, batch_size=self.batch_size, disable=disable)

    def get(self, texts, disable=[], *args):
        return (tuple(arg(doc) for arg in args) for doc in self.__docs(texts, disable))

    def preprocessed_text(self, doc):
        return ' '.join(token.lemma_ for token in doc if not token.is_stop and not token.is_punct)

    def pos_tags(self, doc):
        return [token.pos_ for token in doc]

    def noun_phrases(self, doc):
        return [chunk.text for chunk in doc.noun_chunks]

    def nouns(self, doc):
        return [token.lemma_ for token in doc if token.pos_ == "NOUN"]

    def verbs(self, doc):
        return [token.lemma_ for token in doc if token.pos_ == "VERB"]

    def adjectives(self, doc):
        return [token.lemma_ for token in doc if token.pos_ == "ADJ"]

    def named_entities(self, doc):
        return [(entity.text, entity.label_) for entity in doc.ents]

    def sentences(self, doc):
        return [sent for sent in doc.sents]


class PreprocessExtractor(ABC):

    def __init__(self, preprocessor, disable):
        self.preprocessor = preprocessor
        self.disable = disable

    @abstractmethod
    def get_data(self, texts):
        pass


class SimplePreprocessExtractor(PreprocessExtractor):

    def get_data(self, texts):
        return (PreprocessData(preprocessed_text=preprocessed_text, pos_tags=pos_tags)
                for preprocessed_text, pos_tags in
                self.preprocessor.get(texts, self.disable,
                                      self.preprocessor.preprocessed_text,
                                      self.preprocessor.pos_tags))


class PreprocessData:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)
