class Statistics:
    def __init__(self, data):
        self.data = data
        self.statistics = {'train': self.get_statistics(data.train.X, data.train.Y),
                           'test': self.get_statistics(data.test.X, data.test.Y)}

    def get_statistics(self, X, Y):
        num_of_posts = {}
        num_of_words = {}
        num_users = {}

        minimum_posts = {}
        maximum_posts = {}
        most_common_words = {1: {}, 0: {}}

        for i in range(len(X)):
            num_users[Y[i]] = 1 + num_users.get(Y[i], 0)

            num_of_posts[Y[i]] = len(X[i].posts) + num_of_posts.get(Y[i], 0)

            for post in X[i].posts:
                num_of_words[Y[i]] = len(post.text.split()) + num_of_words.get(Y[i], 0)

                if post.preprocess_data is not None:
                    for word in post.preprocess_data.preprocessed_text.split():
                        most_common_words[Y[i]][word] = 1 + most_common_words[Y[i]].get(word, 0)

        average_prons = {0: int(most_common_words[0]['-PRON-'] / num_users[0]),
                         1: int(most_common_words[1]['-PRON-'] / num_users[1])}

        for y, words in most_common_words.items():
            most_common_words[y] = sorted(words.keys(), key=words.get, reverse=True)

        average_post_length = {0: int(num_of_words[0] / float(num_of_posts[0])),
                               1: int(num_of_words[1] / float(num_of_posts[1]))}
        average_user_words = {0: int(num_of_words[0] / float(num_users[0])),
                              1: int(num_of_words[1] / float(num_users[1]))}
        average_posts = {0: int(num_of_posts[0] / float(num_users[0])), 1: int(num_of_posts[1] / float(num_users[1]))}

        statistics = {'average_posts': average_posts, 'minimum_posts': minimum_posts,
                      'maximum_posts': maximum_posts, 'average_prons': average_prons,
                      'most_common_words': most_common_words, 'average_post_length': average_post_length,
                      'average_user_words': average_user_words}

        return statistics

    def get_train_statistics(self):
        return self.statistics['train']

    def get_test_statistics(self):
        return self.statistics['test']

    def get_statictics_report(self):
        for data_type, stat in self.statistics.items():
            print('For ' + data_type + ' data:')

            print('Number of average posts for non depressed user: ' + str(stat['average_posts'][0]))
            print('Number of average posts for depressed user: ' + str(stat['average_posts'][1]))
            print('Number of average words for non depressed user: ' + str(stat['average_user_words'][0]))
            print('Number of average words for depressed user: ' + str(stat['average_user_words'][1]))
            print('Number of average post length for non depressed user: ' + str(stat['average_post_length'][0]))
            print('Number of average post length for depressed user: ' + str(stat['average_post_length'][1]))
            print('Average number of pronouns for non depressed user: ' + str(stat['average_prons'][0]))
            print('Average number of pronouns for depressed user: ' + str(stat['average_prons'][1]))
            print('20 most common words in depressive posts: ' + str(stat['most_common_words'][1][:20]))
            print('20 most common words in non depressive posts: ' + str(stat['most_common_words'][0][:20]))
            print()
