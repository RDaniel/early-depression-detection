import warnings

from sklearn.pipeline import Pipeline

from feature_extraction import BagOfWords
from ml_model import SupportVectorClassifier, WeightedLogisticRegression, AdaBoost
from model import Data
from postprocessing import graph_reports
from preprocessing import SimplePreprocessExtractor, Preprocessor
from runner import MainRunner
from statistics import Statistics

warnings.filterwarnings("ignore")

data = Data.create("data", extractor=SimplePreprocessExtractor(Preprocessor(), disable=['ner', 'parser']))

print("Preprocessed data ready")

staticstics = Statistics(data)
staticstics.get_statictics_report()

tokenizer = BagOfWords()
sequences = tokenizer.fit_transform(data.train.X)

models = [WeightedLogisticRegression(), SupportVectorClassifier(), AdaBoost()]

for model in models:
    pipeline = Pipeline([("tokenizer", tokenizer), ("model", model)])
    posts_lengths = range(10, 710, 100)

    runner = MainRunner(data, posts_lengths, pipeline)
    reports = runner.run()

    graph_reports(reports, ["f1-score"], ["positive"], posts_lengths)
