import warnings

from sklearn.pipeline import Pipeline

from feature_extraction import SequentialExtractor, TfidfVectorExtractor
from ml_model import XGBoost
from model import Data
from postprocessing import graph_reports
from preprocessing import SimplePreprocessExtractor, Preprocessor
from runner import MainRunner
from statistics import Statistics

warnings.filterwarnings("ignore")

data = Data.create("data", extractor=SimplePreprocessExtractor(Preprocessor(), disable=['ner', 'parser']))

print("Preprocessed data ready")

staticstics = Statistics(data)
staticstics.get_statictics_report()

tokenizer = SequentialExtractor()
sequences = tokenizer.fit_transform(data.train.X)
word_index = tokenizer.tokenizer.word_index

num_words = len(word_index)

feature_extractor = TfidfVectorExtractor()
model = XGBoost()
pipeline = Pipeline([("feature_extractor", feature_extractor), ("model", model)])
posts_lengths = range(150, 860, 50)

runner = MainRunner(data, posts_lengths, pipeline)
reports = runner.run()
graph_reports(reports, ["f1-score"], ["positive"], posts_lengths)
