from abc import ABC, abstractmethod

import numpy as np
from keras import optimizers
from keras.callbacks import EarlyStopping
from keras.layers import LSTM, Activation, Dense, Dropout, Input, Conv1D, MaxPooling1D
from keras.models import Model
from sklearn.ensemble import AdaBoostClassifier
from sklearn.linear_model import LogisticRegressionCV, SGDClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from xgboost import XGBClassifier


class MLModel(ABC):

    @abstractmethod
    def fit(self, X, Y):
        pass

    @abstractmethod
    def predict(self, X):
        pass

    @abstractmethod
    def predict_proba(self, X):
        pass

    @abstractmethod
    def score(self, X, y):
        pass


class WeightedLogisticRegression(MLModel):

    def __init__(self):
        self.model = LogisticRegressionCV(penalty='l2', cv=5, class_weight='balanced', solver='liblinear')

    def fit(self, X, Y):
        self.model = self.model.fit(X, Y)

    def predict(self, X):
        return self.model.predict(X)

    def predict_proba(self, X):
        return self.model.predict_proba(X)

    def score(self, X, Y):
        return self.model.score(X, Y)


class SupportVectorClassifier(MLModel):

    def __init__(self):
        svc = SVC(gamma=0.6, class_weight='balanced', probability=True)
        self.model = GridSearchCV(estimator=svc, param_grid=[{'C': [0.001, 0.01, 0.1, 1, 10],
                                                              'gamma': [0.001, 0.01, 0.1, 1]}], n_jobs=-1)

    def fit(self, X, Y):
        self.model = self.model.fit(X, Y)

    def predict(self, X):
        return self.model.predict(X)

    def predict_proba(self, X):
        return self.model.predict_proba(X)

    def score(self, X, Y):
        return self.model.score(X, Y)


class AdaBoost(MLModel):

    def __init__(self):
        abc = AdaBoostClassifier()
        self.model = GridSearchCV(estimator=abc, param_grid=[{'n_estimators': np.arange(50, 500, 50),
                                                              'learning_rate': np.linspace(0.1, 1, 10)}], n_jobs=-1)

    def fit(self, X, Y):
        self.model = self.model.fit(X, Y)

    def predict(self, X):
        return self.model.predict(X)

    def predict_proba(self, X):
        return self.model.predict_proba(X)

    def score(self, X, Y):
        return self.model.score(X, Y)


class RNN(MLModel):

    def __init__(self, input_len, embedding_layer, batch_size=32, epochs=10, validation_split=0.2,
                 callback=EarlyStopping(monitor='val_loss', min_delta=0.00001, patience=3)):
        inputs = Input(name='inputs', shape=[input_len], dtype='int32')
        layer = embedding_layer(inputs)
        layer = Conv1D(filters=32, kernel_size=5, padding='same', activation='relu')(layer)
        layer = MaxPooling1D(pool_size=2)(layer)
        layer = LSTM(64)(layer)
        layer = Dropout(0.5)(layer)
        layer = Dense(32)(layer)
        layer = Dropout(0.5)(layer)
        layer = Activation('relu')(layer)
        layer = Dense(1, name='out_layer')(layer)
        layer = Activation('sigmoid')(layer)

        self.model = Model(inputs=inputs, outputs=layer)
        self.model.summary()
        optimizer = optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        self.model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['acc'])

        self.batch_size = batch_size
        self.epochs = epochs
        self.validation_split = validation_split
        self.callback = callback

    def fit(self, X, Y):
        self.model.fit(X, Y, batch_size=self.batch_size, epochs=self.epochs, validation_split=self.validation_split,
                       callbacks=[self.callback])

    def predict(self, X):
        predict_proba = self.predict_proba(X)
        predict_proba[predict_proba >= 0.5] = 1
        predict_proba[predict_proba < 0.5] = 0

        return predict_proba

    def predict_proba(self, X):
        return self.model.predict(X, batch_size=self.batch_size)

    def score(self, X, y):
        return self.model.evaluate(X, y, batch_size=self.batch_size)


class NaiveBayes(MLModel):

    def __init__(self):
        self.model = GaussianNB()

    def fit(self, X, Y):
        try:
            X = X.toarray()
        except AttributeError:
            pass
        finally:
            self.model = self.model.fit(X, Y)

    def predict(self, X):
        try:
            X = X.toarray()
        except AttributeError:
            pass
        finally:
            return self.model.predict(X)

    def predict_proba(self, X):
        try:
            X = X.toarray()
        except AttributeError:
            pass
        finally:
            return self.model.predict_proba(X)

    def score(self, X, Y):
        try:
            X = X.toarray()
        except AttributeError:
            pass
        finally:
            return self.model.score(X, Y)


class SGD(MLModel):

    def __init__(self, maxiter=1000, loss='hinge'):
        self.classifier = SGDClassifier(max_iter=maxiter, loss=loss)

    def fit(self, X, y=None):
        self.classifier = self.classifier.fit(X, y)

    def predict(self, X):
        return self.classifier.predict(X)

    def predict_proba(self, X):
        return self.classifier.predict_proba(X)

    def score(self, X, y):
        return self.classifier.score(X, y)


class XGBoost(MLModel):

    def __init__(self):
        gradient_boosting = XGBClassifier(max_depth=3, n_estimators=200)
        self.clf = GridSearchCV(gradient_boosting,
                                param_grid=[{'max_depth': range(2, 7, 2), 'n_estimators': range(50, 251, 50)}],
                                scoring="neg_log_loss",
                                n_jobs=-1)

    def fit(self, X, y=None):
        self.clf = self.clf.fit(X, y)

    def predict(self, X):
        return self.clf.predict(X)

    def predict_proba(self, X):
        return self.clf.predict_proba(X)

    def score(self, X, y=None):
        return self.clf.score(X, y)


class AlphaEnsemble(MLModel):

    def __init__(self):
        svc = SVC(gamma=0.6, class_weight={0: .05, 1: .95}, probability=True)
        gradient_boosting = XGBClassifier(max_depth=3, n_estimators=200)
        log_reg = LogisticRegressionCV(penalty='l2', cv=5, class_weight={0: .05, 1: .95}, solver='liblinear')
        self.classifiers = [
            GridSearchCV(gradient_boosting,
                         param_grid=[{'max_depth': range(2, 9, 2), 'n_estimators': range(50, 201, 50)}]
                         , scoring="neg_log_loss", n_jobs=-1),
            GridSearchCV(estimator=svc,
                         param_grid=[{'C': [0.001, 0.1, 1, 10, 100],
                                      'gamma': [0.001, 0.01, 1, 10, 10]}],
                         n_jobs=-1),
            GridSearchCV(estimator=log_reg,
                         param_grid=[{'Cs': [[0.0001], [0.001], [0.1], [1], [10], [100]]}],
                         n_jobs=-1)
        ]

    def fit(self, X, Y):
        classifiers = []
        for classifier in self.classifiers:
            classifiers.append(classifier.fit(X, Y))

        self.classifiers = classifiers

    def predict(self, X):
        predict = self.predict_proba(X)
        predict[predict >= 0.5] = 1
        predict[predict < 0.5] = 0

        return predict

    def predict_proba(self, X):
        predict_proba = np.zeros(shape=1)
        for classifier in self.classifiers:
            predict_proba = predict_proba + classifier.predict_proba(X)[:, 1]
        predict_proba = predict_proba / len(self.classifiers)

        return predict_proba

    def score(self, X, y):
        total_score = 0
        for classifier in self.classifiers:
            total_score += classifier.score(X, y)

        return total_score / len(self.classifiers)
