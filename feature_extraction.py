import os
from abc import ABC, abstractmethod

import numpy as np
from gensim.models import KeyedVectors
from keras.initializers import Constant
from keras.layers import Embedding
from keras.preprocessing import sequence
from keras.preprocessing.text import Tokenizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

from bm25 import BM25Transformer

user_to_post = lambda user: user.get_preprocess_posts()


class FeatureExtractor(ABC):

    @abstractmethod
    def fit_transform(self, X, y=None):
        pass

    @abstractmethod
    def transform(self, X, y=None):
        pass


class BagOfWords(FeatureExtractor):

    def __init__(self):
        self.vectorizer = CountVectorizer(max_features=5000)

    def fit_transform(self, X, y=None):
        return self.vectorizer.fit_transform(map(user_to_post, X))

    def transform(self, X, y=None):
        return self.vectorizer.transform(map(user_to_post, X))


class TfidfVectorExtractor(FeatureExtractor):

    def __init__(self):
        self.vectorizer = TfidfVectorizer()

    def fit_transform(self, X, y=None):
        return self.vectorizer.fit_transform(map(user_to_post, X))

    def transform(self, X, y=None):
        return self.vectorizer.transform(map(user_to_post, X))

    def get_feature_names(self):
        return self.vectorizer.get_feature_names()


class SequentialExtractor(FeatureExtractor):

    def __init__(self, max_words=20000, maxlen=1000):
        self.tokenizer = Tokenizer(max_words)
        self.max_words = max_words
        self.maxlen = maxlen

    def fit_transform(self, X, y=None):
        self.tokenizer.fit_on_texts(map(user_to_post, X))

        sequences = self.tokenizer.texts_to_sequences(map(user_to_post, X))
        sequences_matrix = sequence.pad_sequences(sequences, maxlen=self.maxlen)

        return sequences_matrix

    def transform(self, X, y=None):
        sequences = self.tokenizer.texts_to_sequences(map(user_to_post, X))
        sequences_matrix = sequence.pad_sequences(sequences, maxlen=self.maxlen)

        return sequences_matrix


class Word2VecExtractor(FeatureExtractor):

    def __init__(self, w2v_dir, w2v_file, trainable=False, transformers=[lambda x, y: x + y]):
        self.model = KeyedVectors.load_word2vec_format(os.path.join(w2v_dir, w2v_file), binary=True)
        self.trainable = trainable
        self.transformers = transformers
        self.VECTOR_SIZE = 200

    def fit_transform(self, X, y=None):
        self.embedding_layer = self.model.get_keras_embedding(self.trainable)
        return self.transform(X)

    def transform(self, X, y=None):
        X = list(map(user_to_post, X))
        res = np.zeros(len(X), self.VECTOR_SIZE * len(self.transformers))

        for i, row in enumerate(X):
            for word in row.split():
                for j, transformer in enumerate(self.transformers):
                    place = j * self.VECTOR_SIZE
                    vec = res[i, place: place + self.VECTOR_SIZE]
                    try:
                        res[i, place: place + self.VECTOR_SIZE] = transformer(vec, self.model.word_vec(word))
                    except KeyError:
                        pass
        return res


class GloVeExtractor(FeatureExtractor):

    def __init__(self, glove_dir, glove_file, vector_size, word_index, trainable=False,
                 transformers=[lambda x, y: x + y]):
        self.glove_file = os.path.join(glove_dir, glove_file)
        self.vector_size = vector_size
        self.glove_index = {}
        self.transformers = transformers
        self.word_index = word_index
        self.trainable = trainable

    def fit_transform(self, X, y=None):
        with open(self.glove_file, encoding="utf8") as file:
            for line in file:
                values = line.split()
                word = values[0]
                coeffs = np.asarray(values[1:], dtype=np.float32)
                self.glove_index[word] = coeffs

        self.embedding_matrix = np.zeros((len(self.word_index) + 1, self.vector_size))

        for word, i in self.word_index.items():
            embedding_vector = self.glove_index.get(word)
            if embedding_vector is not None:
                self.embedding_matrix[i] = embedding_vector

        self.embedding_layer = Embedding(input_dim=self.embedding_matrix.shape[0],
                                         output_dim=self.embedding_matrix.shape[1],
                                         embeddings_initializer=Constant(self.embedding_matrix),
                                         trainable=self.trainable)
        return self.transform(X)

    def transform(self, X, y=None):
        X = list(map(user_to_post, X))
        res = np.zeros((len(X), self.vector_size * len(self.transformers)))
        default = np.zeros(self.vector_size)

        for i, row in enumerate(X):
            for word in row.split():
                index = self.word_index.get(word)
                embed_vec = self.embedding_matrix[index] if index is not None else default

                for j, transformer in enumerate(self.transformers):
                    place = j * self.vector_size
                    vec_at_place = res[i, place: place + self.vector_size]
                    res[i, place: place + self.vector_size] = transformer(vec_at_place, embed_vec)

        return res


class GloVeWeightedExtractor(FeatureExtractor):

    def __init__(self, glove_dir, glove_file, vector_size, word_index, trainable=False,
                 transformers=[lambda x, y: x + y]):
        self.glove_file = os.path.join(glove_dir, glove_file)
        self.vector_size = vector_size
        self.glove_index = {}
        self.transformers = transformers
        self.word_index = word_index
        self.trainable = trainable
        self.tf_idf_extractor = TfidfVectorExtractor()

    def fit_transform(self, X, y=None):
        with open(self.glove_file, encoding="utf8") as file:
            for line in file:
                values = line.split()
                word = values[0]
                coeffs = np.asarray(values[1:], dtype=np.float32)
                self.glove_index[word] = coeffs

        self.embedding_matrix = np.zeros((len(self.word_index) + 1, self.vector_size))

        for word, i in self.word_index.items():
            embedding_vector = self.glove_index.get(word)
            if embedding_vector is not None:
                self.embedding_matrix[i] = embedding_vector

        self.embedding_layer = Embedding(input_dim=self.embedding_matrix.shape[0],
                                         output_dim=self.embedding_matrix.shape[1],
                                         embeddings_initializer=Constant(self.embedding_matrix),
                                         trainable=self.trainable)

        self.tf_idf_extractor.fit_transform(X, y)
        return self.transform(X)

    def transform(self, X, y=None):
        tf_idf_vectors = self.tf_idf_extractor.transform(X, y)
        words = self.tf_idf_extractor.get_feature_names()

        X = list(map(user_to_post, X))
        res = np.zeros((len(X), self.vector_size * len(self.transformers)))
        default = np.zeros(self.vector_size)

        for i, row in enumerate(X):
            row_tfidf_vector = tf_idf_vectors[i]
            tfidf_scores = {}

            for idx in range(len(row_tfidf_vector.data)):
                tfidf_scores[words[row_tfidf_vector.indices[idx]]] = row_tfidf_vector.data[idx]

            for word in row.split():
                index = self.word_index.get(word)
                tfidf = tfidf_scores.get(word, 0)
                embed_vec = tfidf * self.embedding_matrix[index] if index is not None else default

                for j, transformer in enumerate(self.transformers):
                    place = j * self.vector_size
                    vec_at_place = res[i, place: place + self.vector_size]
                    res[i, place: place + self.vector_size] = transformer(vec_at_place, embed_vec)

        return res


class BM25Extractor(FeatureExtractor):

    def __init__(self):
        self.count_vectorizer = CountVectorizer()
        self.vectorizer = BM25Transformer()

    def fit_transform(self, X, y=None):
        count_matrix = self.count_vectorizer.fit_transform(map(user_to_post, X))
        return self.vectorizer.fit_transform(count_matrix)

    def transform(self, X, y=None):
        count_matrix = self.count_vectorizer.transform(map(user_to_post, X))
        return self.vectorizer.transform(count_matrix)
