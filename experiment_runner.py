import pickle
import sys

from sklearn.pipeline import Pipeline

from feature_extraction import *
from ml_model import *
from model import Data, DataSet
from model_selection import KFoldClass
from postprocessing import report_string
from preprocessing import SimplePreprocessExtractor, Preprocessor


def get_lengths(default_values=(10, 710, 100)):
    retvals = []
    try:
        for string in sys.argv[3:]:
            retvals.append(int(string))
        return tuple(retvals) if len(retvals) == 3 else default_values
    except (ValueError, TypeError, IndexError):
        return default_values


extractors = {}


def initialize_extractors(data):
    tokenizer = SequentialExtractor()
    tokenizer.fit_transform(data.train.X + data.test.X)
    word_index = tokenizer.tokenizer.word_index

    glove = lambda: GloVeExtractor("glove.6B", "glove.6B.100d.txt", 100, word_index)
    w2v = lambda: Word2VecExtractor("word2vec", "vectors.bin")
    tfidf = TfidfVectorExtractor

    extractors["glove"] = glove
    extractors["word2vec"] = w2v
    extractors["tfidf"] = tfidf
    extractors["bag_of_words"] = BagOfWords
    extractors["glove_weighted"] = lambda: GloVeWeightedExtractor("glove.6B", "glove.6B.100d.txt", 100, word_index)
    extractors["bm25"] = BM25Extractor


models = {}


def initialize_models():
    models["logistic"] = WeightedLogisticRegression
    models["bayes"] = NaiveBayes
    models["xgboost"] = XGBoost
    models["adaboost"] = AdaBoost
    models["ensemble"] = AlphaEnsemble
    models["svm"] = SupportVectorClassifier


if __name__ == "__main__":

    config = sys.argv[1]

    data = Data.create("data", extractor=SimplePreprocessExtractor(Preprocessor(), disable=['ner', 'parser']))
    data = Data(DataSet(data.train.X + data.test.X, data.train.Y + data.test.Y), DataSet([], []))

    initialize_extractors(data)
    initialize_models()

    if config == "-h":
        print("Models:")
        print(", ".join(models.keys()))
        print("Extractors:")
        print(", ".join(extractors.keys()))
        exit()

    n_of_folds = int(sys.argv[2])

    try:
        res_file = sys.argv[3]
    except IndexError:
        res_file = "results.pickle"

    start, end, rnge = get_lengths()

    posts_length = range(start, end, rnge)

    pipelines = []

    model_extractor = []

    with open(config) as config_file:
        for line in config_file:
            params = line.rstrip().split(";")
            model = (params[-1], models[params[-1]]())
            extracts = [(param, extractors[param]()) for param in params[:-1]]
            model_extractor.append(tuple(params))
            pipeline = Pipeline([*extracts, model])
            print(pipeline)
            pipelines.append(pipeline)

    kfold = KFoldClass(n_of_folds)
    res = kfold.run(data, posts_length, pipelines)

    with open(res_file, "wb") as results:
        pickle.dump(res, results)

    for j, me in enumerate(model_extractor):
        report_2D = [kf[j] for kf in res]
        with open("_".join(me) + ".txt", "w") as text_res, open("_".join(me) + ".pickle", "wb") as repro:
            pickle.dump(report_2D, repro)
            for i, reports in enumerate(report_2D):
                text_res.write(f"{i}. fold:\n")
                for report in reports:
                    text_res.write(report_string(report) + "\n")
