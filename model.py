import os
import pickle

from progress.bar import IncrementalBar
from sklearn.model_selection import train_test_split

import loader


def transform_user(user, posts_length):
    return User(user.id, user.posts[:min(posts_length, len(user.posts))])


class User:
    def __init__(self, id, posts):
        self.id = id
        self.posts = posts

    def __str__(self):
        return "ID = {}\nPosts = {}".format(self.id, self.posts)

    def __repr__(self):
        return str(self)

    def get_preprocess_posts(self):
        return ' '.join([post.preprocess_data.preprocessed_text for post in self.posts
                         if post.preprocess_data is not None])

    def get_posts(self):
        return ' '.join([post.text for post in self.posts])


class Post:
    def __init__(self, title, date, text):
        self.title = title
        self.date = date
        self.text = text
        self.preprocess_data = None

    def __str__(self):
        return "\nTITLE = {}\nDATE = {}\nTEXT = {}".format(self.title, self.date, self.text)

    def __repr__(self):
        return str(self)


class DataSet:

    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

        self.size = len(self.X)

    def clear(self, min_length=3):
        for x in self.X:
            posts = []
            for post in x.posts:
                if post.preprocess_data is None or len(post.preprocess_data.preprocessed_text.split()) < min_length:
                    continue
                posts.append(post)

            x.posts = posts


class Data:
    __CACHE_NAME = "./data.pickle"

    def __init__(self, train, test):
        self.train = train
        self.test = test
        self.post_size = 0
        for sample in self:
            self.post_size += sum(1 for post in sample.posts if post.text != "" or post.title != "")

    @staticmethod
    def create(data_dir, use_cached=True, extractor=None):
        """Method for loading and preprocessing the data."""
        if not os.path.exists(Data.__CACHE_NAME) and use_cached:
            data = Data.load_data(data_dir)

            if extractor:
                bar = IncrementalBar("Processing", max=data.post_size, suffix='%(percent).1f%% | %(elapsed_td)s')
                data.preprocess(extractor, after_post_action=bar.next)
                bar.finish()

            data.cache()
        else:
            with open(Data.__CACHE_NAME, "rb") as pickle_data:
                data = pickle.load(pickle_data)

        return data

    @staticmethod
    def load_data(data_dir):
        """Method for loading the data."""
        train_dir = os.path.join(data_dir, "train")
        positive_train = loader.load_from_directory(os.path.join(train_dir, "positive"))
        negative_train = loader.load_from_directory(os.path.join(train_dir, "negative"))

        train_X = positive_train + negative_train
        train_Y = [1] * len(positive_train) + [0] * len(negative_train)

        test_dir = os.path.join(data_dir, "test")
        positive_test = loader.load_from_directory(os.path.join(test_dir, "positive"))
        negative_test = loader.load_from_directory(os.path.join(test_dir, "negative"))

        test_X = positive_test + negative_test
        test_Y = [1] * len(positive_test) + [0] * len(negative_test)

        return Data(DataSet(train_X, train_Y), DataSet(test_X, test_Y))

    def get_first_n_posts(self, n):
        train_X = list(map(lambda user: transform_user(user, n), self.train.X))
        test_X = list(map(lambda user: transform_user(user, n), self.test.X))

        return Data(DataSet(train_X, self.train.Y), DataSet(test_X, self.test.Y))

    def get_chunks(self, n):
        train_X, train_Y = self.get_chunks_from_user(n, self.train.X, self.train.Y)
        test_X, test_Y = self.get_chunks_from_user(n, self.test.X, self.test.Y)

        return Data(DataSet(train_X, train_Y), DataSet(test_X, test_Y))

    def get_chunks_from_user(self, n, users_X, users_Y, chunk_size=250):
        X = []
        Y = []

        for user, label in zip(users_X, users_Y):
            m = len(user.posts) if label else n
            posts = user.posts[:min(m, len(user.posts))]

            x = ''
            chunks = []

            for post in posts:
                x = x + ' ' + (post.text if post.title == "" else post.title)
                if len(x.split()) > chunk_size:
                    chunk = ' '.join(x.replace('\n', ' ').replace('  ', ' ').split()[:chunk_size])
                    X.append(chunk)
                    Y.append(label)
                    chunks.append(chunk)
                    x = ''

        return X, Y

    def random_split(self, test_size):
        X = self.train.X + self.test.X
        Y = self.train.Y + self.test.Y
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=test_size, random_state=50)

        return Data(DataSet(X_train, y_train), DataSet(X_test, y_test))

    def cache(self):
        with open(Data.__CACHE_NAME, "wb") as pickle_data:
            pickle.dump(self, pickle_data)

    def __iter__(self):
        return iter(self.get_all_samples())

    def get_all_samples(self):
        return self.train.X + self.test.X

    def preprocess(self, preprocess_extractor, after_post_action):
        texts = []
        posts = []
        for sample in self:
            post_addition = [post for post in sample.posts if
                             post.text != "" or post.title != ""]  # problem kod koristenja nlp.pipe
            texts.extend([post.text if post.title == "" else post.title for post in post_addition])
            posts.extend(post_addition)

        for post, processed_data in zip(posts, preprocess_extractor.get_data(texts)):
            post.preprocess_data = processed_data
            after_post_action()
