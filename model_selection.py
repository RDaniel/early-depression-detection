import itertools as it

from scipy.stats import ttest_ind
from sklearn.model_selection import StratifiedKFold

from model import Data, DataSet
from runner import MainRunner


class KFoldClass:

    def __init__(self, n_folds, shuffle=False):
        self.kfold = StratifiedKFold(n_folds, shuffle)

    def split(self, data):
        datas = []
        for indices_train, indices_test in self.kfold.split(data.train.X, data.train.Y):
            train_x = []
            train_y = []
            for i in indices_train:
                train_x.append(data.train.X[i])
                train_y.append(data.train.Y[i])
            test_x = []
            test_y = []
            for j in indices_test:
                test_x.append(data.train.X[j])
                test_y.append(data.train.Y[j])
            datas.append(Data(DataSet(train_x, train_y), DataSet(test_x, test_y)))
        return datas

    def run(self, data, posts_lengths, pipelines):
        all_results = []

        for i, split_data in enumerate(self.split(data)):
            fold_results = []

            for pipeline in pipelines:
                reports = MainRunner(split_data, posts_lengths, pipeline).run()
                fold_results.append(reports)

            all_results.append(fold_results)

        return all_results

    @staticmethod
    def statistic_t_test(kfold_res, clz, measures):
        # TODO izbjeci iteraciju po listi rezultata, koristiti samo vrijednosti iz folda
        results = {}
        for measure in measures:
            for first, second in it.combinations(range(len(kfold_res[0])), 2):
                print(len(kfold_res[0][0]))
                results[(first, second)] = []
                for index_post_length in range(len(kfold_res[0][0])):
                    a = [kfold[first][index_post_length][clz][measure] for kfold in kfold_res]
                    b = [kfold[second][index_post_length][clz][measure] for kfold in kfold_res]
                    results[(first, second)].append(ttest_ind(a, b, equal_var=False))
        return results
